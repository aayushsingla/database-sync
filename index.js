const fetch = require('node-fetch')
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/";
let lastJSON = ""
const crypto = require('crypto')

MongoClient.connect(url,(err,database) =>{
    if (err) return console.log(err)
    const db = database.db('hackathon');
    getData(db)
    setInterval(() => {
        getData(db)
    }, 10000)
})

function checkLoop(oldrecord, newrecord, id, logobject) {
    console.log("log object is ", logobject.logobject)
    for( key in newrecord ) {
        //    console.log(`Key: ${key} ; newrecord value: ${newrecord[key]} ; oldrecord value: ${oldrecord[key]}`)
        if(typeof newrecord[key] === "object" && typeof oldrecord[key] === "object" && newrecord[key] !== null) {
            logobject.logobject += encodeURIComponent(key)+" -> "
            checkLoop(oldrecord[key], newrecord[key], id, logobject)
            logobject.logobject = ""
        } else if(newrecord[key] !== oldrecord[key]) {
            console.log(`{action:"change",  message: "'${encodeURIComponent(key)}' value is changed from ${encodeURIComponent(oldrecord[key])} to ${encodeURIComponent(newrecord[key])}", path: "${logobject.logobject}${encodeURIComponent(key)}"`)
            oldrecord[key] = newrecord[key]
            logobject.logobject = ""
        }
    } 
}

async function getData(db) {
   // console.log("Downloading..")
    const recordsToUpdate = []
    const collectionName = "records_2"
   /// const p = performance.now()
    const fetchReq = await fetch('http://c9d03f6e.ngrok.io/database.json')
    const data = await fetchReq.json()
  //  console.log("Ready..")
    const thisJSON = crypto.createHash('md5').update(JSON.stringify(data)).digest("hex")

    if(thisJSON === lastJSON) {
       // console.log("Data same - md5")
        return
    }

    lastJSON = thisJSON

    const clientList = data.GetClientsResult.Clients.Client
    let oldrecords = {}
    try {
        oldrecords = (await db.collection(collectionName).findOne()).GetClientsResult.Clients.Client
    } catch(e) {
        // new database
    }
    
    for(let i = 0;i<clientList.length; i++) {
        const newrecord = clientList[i]
        const id = newrecord.UniqueID
        const oldrecord = oldrecords[id]
//console.log(clientList.length)
        if(!oldrecord) {

            var key2 = 'GetClientsResult.Clients.Client.'+id
            const updatedValue = {}
            updatedValue[key2] = newrecord
            await db.collection(collectionName).updateOne({}, { $set: updatedValue })
            console.log("{action: 'insert', message: 'Inserted a new record'}")
            continue
        }

        const oldrecordbackup = JSON.stringify(oldrecord)

        let logobject = {logobject: ""}

        checkLoop(oldrecord, newrecord, id, logobject)

        //console.log(newrecord)
  
        
        //console.log(oldrecordbackup, JSON.stringify(oldrecord))
        if(oldrecordbackup !== JSON.stringify(oldrecord)) {

            recordsToUpdate.push(oldrecord)
        }
        
    }

    for(let k=0;k<recordsToUpdate.length;k++) {
        const record = recordsToUpdate[k]
        var key2 = 'GetClientsResult.Clients.Client.'+record.UniqueID
        const filter = {}
        const updatedValue = {}
        updatedValue[key2] = record
        //console.log(filter, updatedValue)
        try {
            await db.collection(collectionName).updateOne({}, { $set: updatedValue })
        } catch(e) {
            console.log(e)
        }
        //console.log(await db.collection('records').findOne({'GetClientsResult.Clients.Client.100015484.UniqueID': '100015484'})) //, { "$set": updatedValue })
    }

    if(recordsToUpdate.length > 0) {
        console.log(`{action:'update', message: '${recordsToUpdate.length} records updated in database'}`)
    } else {
        //console.log("database already in sync")
    }
    
}